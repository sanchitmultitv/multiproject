import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// //  import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { FormsModule,  } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {WebcamModule} from 'ngx-webcam';
import {SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { ThanksComponent } from './thanks/thanks.component';
import { GameComponent } from './@core/game/game.component';
import { DemoComponent } from './@core/demo/demo.component';
import { ToastrModule } from 'ngx-toastr';
// import { ProductsComponent } from './@core/products/products.component';
import { ExhibitionHallComponent } from './@core/exhibition-hall/exhibition-hall.component';
// import { EventHallTwoComponent } from './@core/event-hall-two/event-hall-two.component';
import { NetworkingLoungeComponent } from './@core/networking-lounge/networking-lounge.component';
//  import { from } from 'core-js/fn/array';
// import { DatepickerModule } from 'ng2-datepicker';
// import { from } from 'core-js/fn/array';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const data: SocketIoConfig ={ url : 'https://belive.multitvsolution.com:8030', options: {} };


@NgModule({
  declarations: [
    AppComponent,
    ThankyouComponent,
    ThanksComponent,
    GameComponent,
    DemoComponent,
    ExhibitionHallComponent,
    NetworkingLoungeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    WebcamModule,
    SocketIoModule.forRoot(data),
     ToastrModule.forRoot(),
     ReactiveFormsModule,
    //  DatepickerModule,
     
    NgbModule,
  ],
  
  // exports: [
  //   DatepickerModule,
  //   ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GameComponent } from '../@core/game/game.component';
import { LayoutComponent } from './layout.component';
import{DemoComponent }  from '../@core/demo/demo.component'
import{ExhibitionHallComponent}  from '../@core/exhibition-hall/exhibition-hall.component';
import { EventHallTwoComponent } from '../@core/event-hall/components/event-hall-two/event-hall-two.component';
import { sequence } from '@angular/animations';
import { NetworkingLoungeComponent } from '../@core/networking-lounge/networking-lounge.component';
import { AppoinmentsComponent } from './appoinments/appoinments.component';
import { AttendeesComponent } from './attendees/attendees.component';
// import { ScheduleCallComponent } from './components/schedule-call/schedule-call.component';


const routes: Routes = [
  {
    path:'',
    component: LayoutComponent,
    children:[
      { path: '', redirectTo: 'lobby', pathMatch: 'prefix' },
      {path: 'lobby', loadChildren: ()=> import('../@core/lobby/lobby.module').then(m => m.LobbyModule)},
      {path: 'exhibition-hall', loadChildren: ()=> import('../@core/exhibition-hall/exhibition-hall.module').then(m => m.ExhibitionHallModule)},
      {path: 'photobooth', loadChildren: ()=> import('../@core/photobooth/photobooth.module').then(m => m.PhotoboothModule)},
      {path: 'eventhall', loadChildren: ()=> import('../@core/event-hall/event-hall.module').then(m => m.EventHallModule)},
        {path:'eventhalltwo',component:EventHallTwoComponent},
      {path: 'gratitudewall', loadChildren: ()=> import('../@core/gratitude-wall/gratitude-wall.module').then(m => m.GratitudeWallModule)},
      { path: 'gamezone', component:GameComponent},
      { path: 'demo', component:DemoComponent},
      { path: 'networking-lounge', component:NetworkingLoungeComponent},
      { path: 'appoinments', component: AppoinmentsComponent},
      { path: 'attendees', component: AttendeesComponent},

      // { path: 'sedual', component:ScheduleCallComponent},
      // {path: 'speakers', loadChildren: ()=> import('../@core/speaker-profile/speaker-profile.module').then(m => m.SpeakerProfileModule)},
      // {path: 'products', loadChildren: ()=> import('../@core/products/products.module').then(m => m.ProductsModule)},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }

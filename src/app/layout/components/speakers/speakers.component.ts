import { Component, OnInit } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {
  points=[];
  speaker;
  currentDate;
  constructor() { }

  ngOnInit(): void {
    let left:any = document.getElementById('previousleft');
    left.style.display = 'none';
    let date = new Date().getDate();
    this.currentDate = date;
    if(date <= 24){
      this.speaker = '24';
      for (let i = 0; i < 12; i++) {
        this.points.push(i+1);
      }
    }
    if(date == 25 ){
      this.points = [];
      this.speaker = '25';
      for (let i = 12; i < 25; i++) {
        this.points.push(i+1);
      }
    }
    if(date >= 26){
      this.speaker = '26';
    }
  }
  closeParentModal(){
    $('#speakersModal').modal('hide');
  }
  imgId;
  openProfile(prof){
    this.imgId = prof;
    $('#speakersModal').modal('hide');
    $('#speakerProfileModal').modal('show');
  }
  closeChildModal(){
    $('#speakersModal').modal('hide');
    $('#speakerProfileModal').modal('hide');
  }
  showprevious(){
    let left:any = document.getElementById('previousleft');
    let right:any = document.getElementById('nextright');
    if(this.speaker === '25'){
      this.speaker = '24'
      left.style.display = 'none';
      this.points = [];
      for (let i = 0; i < 13; i++) {
        this.points.push(i+1);
      }
    }
    if(this.speaker === '26'){
      this.speaker = '25'
      right.style.display = 'block';
      this.points = [];
      for (let i = 12; i < 25; i++) {
        this.points.push(i+1);
      }
    }
  }
  shownext(){
    let right:any = document.getElementById('nextright');
    let left:any = document.getElementById('previousleft');
    left.style.display='block';
    if(this.speaker === '25'){
      this.speaker = '26';
      right.style.display = 'none';
      this.points = [];
      for (let i = 25; i < 39; i++) {
        this.points.push(i+1);
      }
    }
    if(this.speaker === '24'){
      this.speaker = '25'
      left.style.display = 'block';
      this.points = [];
      for (let i = 12; i < 25; i++) {
        this.points.push(i+1);
      }
    }
  }
}

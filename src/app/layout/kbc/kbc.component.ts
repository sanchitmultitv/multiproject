import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
declare var $: any;
@Component({
  selector: 'app-kbc',
  templateUrl: './kbc.component.html',
  styleUrls: ['./kbc.component.scss']
})
export class KbcComponent implements OnInit {
  interval;
  player:any
  user_name;
  newWidth;
  newHeight;
   videoPlayer;
   quizData: any = [];
  quizlist = [];
  summaryList = [];
  summaryBoolean = false;
  index = 0;
  sumIndex = 0;
  user_id: any;
  quizStatus = 'Quiz';
  correctAnsCount = 0;
  liveMsg= false;

  constructor(private _fd: FetchDataService, private chat: ChatService) { }

  ngOnInit(): void {
    this.user_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.getQuizlist();
    
  }
  getQuizlist() {
    let event_id = 164;
    this._fd.getQuizList(event_id).subscribe((res: any) => {
      this.quizlist = res.result;
      console.log('dddd', res)
    });
  }
  loadData() {
    this._fd.getQuiz().subscribe((data: any) => {
      this.quizData = data.results;
      for (let i = 0; i < this.quizData.length; i++) {
        for (let j = 0; j < this.quizData[i]['incorrect_answers'].length; j++) {
          this.quizData[i].option = this.quizData[i]['incorrect_answers'];
        }
        this.quizData[i].option.push(this.quizData[i]['correct_answer']);
        // console.log('time', this.quizData[i])          
      }
    });
  }

  selectedOption;
  optionIndex = -1;
  color;
  noColor = '#27272d';
  nextQuestion(index, j, quiz) {
    let quiz_id: any = quiz.id;
    let answer: any = quiz.correct_answer;
    this.optionIndex = j;
    this.color = 'green';
    const formData = new FormData();
    formData.append('user_id', this.user_id);
    formData.append('quiz_id', quiz_id);
    formData.append('answer', answer);
    this._fd.postSubmitQuiz(formData).subscribe(res=>{
      console.log('ind', index, this.quizlist.length);
    });
    setTimeout(() => {
      this.index = index + 1;
      this.optionIndex = -1;
      this.color = '#27272d';
      if (this.quizlist.length === this.index){
        let event_id = 164;
        this._fd.getSummaryQuiz(event_id, this.user_id).subscribe(resp=>{
          this.summaryBoolean = true;
          this.summaryList = resp.result;
          this.quizStatus = 'Quiz Summary';
        });
      }
    }, 1000);
  }

  closePopup() {
    $('.quizModal').modal('hide');
  }

}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KbcComponent } from './kbc.component';

describe('KbcComponent', () => {
  let component: KbcComponent;
  let fixture: ComponentFixture<KbcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KbcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KbcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

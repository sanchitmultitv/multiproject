import { Component, OnInit, Renderer2 } from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-schedule-call',
  templateUrl: './schedule-call.component.html',
  styleUrls: ['./schedule-call.component.scss']
})
export class ScheduleCallComponent implements OnInit {
  model: NgbDateStruct;
  date: {year: number, month: number};
timeVal;
msg;
exhibitionName;
newTimeSlots
uids;
constructor(private calendar: NgbCalendar,private route: ActivatedRoute, private renderer: Renderer2,private _fd : FetchDataService,private toastr: ToastrService) { }

  ngOnInit(): void {
      // Data Picker Initialization

  }
  
  
  
  selectToday() {
    this.model = this.calendar.getToday();
    console.log(this.model);
  }
  closePopup() {
    $('.scheduleCallmodal').modal('hide');
  }
  getTime(event: any, valClass, time) {
 console.log(time);
this.timeVal = time;
const hasClass = event.target.classList.contains(valClass);
$(".time-list li a.active").removeClass("active");
// adding classname 'active' to current click li
this.renderer.addClass(event.target, valClass);
// if (hasClass) {
//   //alert('has')
//       this.renderer.removeClass(event.target, valClass);
//     } else {
//      // alert(valClass)
//       this.renderer.addClass(event.target, valClass);
//     }
  }

  confirm() {
    // alert(this.dateModel);
 //console.log('val',dp);
 this.route.paramMap.subscribe(params => {
  this.exhibitionName = params.get('exhibitName');
  let data = JSON.parse(localStorage.getItem('virtual'));
  console.log(this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
  const CallData = new FormData();
  CallData.append('exhibition_id', this.exhibitionName);
  CallData.append('user_id', data.id);
  CallData.append('time', this.model.year+'-'+this.model.month+'-'+this.model.day+' '+this.timeVal);
  this._fd.schdeuleAcall(CallData).subscribe(res=>{
   console.log(res);
   if(res.code == 1){
    this.toastr.success( 'Call scheduled succesfully!');

    let myDate = this.model.year+'-'+this.model.month+'-'+this.model.day;
    //let myDate = '2020-10-25';
    let data = JSON.parse(localStorage.getItem('virtual'));
    this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
       console.log('timeresponse',res);
       this.newTimeSlots=res.result;
     })
    setTimeout(() => {
     $('.scheduleCallmodal').modal('hide');
    }, 2000);
   }
  
 });
 
});
  }






 onDateSelect(dates) {
  console.log('aycycyt',dates.year+'-'+dates.month+'-'+dates.day);
  let myDate = dates.year+'-'+dates.month+'-'+dates.day;
  this.route.paramMap.subscribe(params => {
    this.exhibitionName = params.get('exhibitName');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this._fd.totalTimeSlots(this.exhibitionName,myDate).subscribe(res=>{
      console.log('timeresponse',res);
      this.newTimeSlots=res.result;
    })
  });
  
 }

// onDateSelect(dates) {
//   console.log('aycycyt', dates.year + '-' + dates.month + '-' + dates.day);
//   let myDate = dates.year + '-' + dates.month + '-' + dates.day;
//   //this.route.paramMap.subscribe(params => {
//   //this.exhibitionName = params.get('exhibitName');
//   let data = JSON.parse(localStorage.getItem('virtual'));
//   this._fd.totalTimeSlots(this.uids, myDate).subscribe(res => {
//     console.log('timeresponse', res);
//     this.newTimeSlots = res.result;
//   })
//   //});


// }

}

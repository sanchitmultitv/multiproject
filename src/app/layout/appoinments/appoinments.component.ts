import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-appoinments',
  templateUrl: './appoinments.component.html',
  styleUrls: ['./appoinments.component.scss']
})
export class AppoinmentsComponent implements OnInit {

  user_profile;
  callList:any=[];
  callListattendee:any=[];
  constructor(private _fd:FetchDataService) { }

  ngOnInit(): void {
    this.user_profile = JSON.parse(localStorage.getItem('virtual'));
    this.getScheduleCall();
    this.getScheduleCallatttendee();
  }
  getScheduleCall(){
    this._fd.getScheduleList(this.user_profile.id).subscribe(res=>{
      console.log(res);
      this.callList = res.result;
    })
  }
  getScheduleCallatttendee(){
    this._fd.getScheduleListatttendee(this.user_profile.id).subscribe(res=>{
      console.log(res);
      this.callListattendee = res.result;
    })
  }
}

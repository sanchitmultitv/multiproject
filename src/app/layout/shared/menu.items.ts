export const MenuItems = [
    {
        path: '/lobby',
        icon: 'assets/ipf/icons/new/1.png',
        name: 'lobby',
        analytics: 'click_lobby'
    },
    // {
    //     datatarget: 'gratitudewall',
    //     icon: 'assets/ipf/icons/new/2.png',
    //     name: 'agenda',
    //     analytics: 'click_agenda'
    // },
    {
        datatarget: 'speakersModal',
        icon: 'assets/ipf/icons/new/3.png',
        name: 'speaker profile',
        analytics: 'click_speakerprofile'
    },
    {
        datatarget: 'feedbackModal',
        icon: 'assets/ipf/icons/new/9.png',
        name: 'Agenda',
        analytics: 'click_agenda'
    },
  
    {
        path: '/eventhall',
        icon: 'assets/ipf/icons/new/4.png',
        name: 'conference-One',
        analytics: 'auditorium_226'
    },
    {  path: '/eventhalltwo',
        icon: 'assets/ipf/icons/new/4.png',
        name: 'conference-two',
        analytics: 'auditorium_227'
    },
    {
        path: '/networking-lounge',
        icon: 'assets/ipf/icons/new/7.png',
        name: 'networking-lounge',
        analytics: 'click_networkinglounge'
    },
    {
        path: '/photobooth', 
        icon: 'assets/ipf/icons/new/5.png',
        name: 'photobooth',
        analytics: 'click_photobooth'
    },
    {
        path: '/gratitudewall',
        icon: 'assets/ipf/icons/new/5.png',
        name: 'gallery',
        analytics: 'click_gallery'
    },
    // {
    //     path: '/demo',
    //     icon: 'assets/ipf/icons/new/5.png',
    //     name: 'Demo',
    //     analytics: 'click_gallery'
    // },
    {
             path: '/exhibition-hall/life',
             icon: 'assets/ipf/icons/new/5.png',
             name: 'exhibition-hall',
             analytics: 'click_gallery'
         },
        //  {
        //     datatarget: 'agendaModal',
        //     icon: 'assets/ipf/icons/new/9.png',
        //     name: 'Agenda',
        //     analytics: 'click_agenda'
        // },
    
    {
        path: '/gamezone',
        icon: 'assets/ipf/icons/new/7.png',
        name: 'Games', 
        analytics: 'click_engangementzone'
    },
    {
        datatarget: 'helpDeskModal',
        icon: 'assets/ipf/icons/new/8.png',
        name: 'help desk',
        analytics: 'click_helpdesk'
    },
    {
        path: '/appoinments',
        icon: 'assets/ipf/icons/new/7.png',
        name: 'My Appoinments', 
        analytics: ''
    },
    {
      datatarget: 'attendeesModal',
        icon: 'assets/ipf/icons/new/9.png',
        name: 'Attendees',
        analytics: 'click_Attendees'
    },
    // {
    //     path: '/attendees',
    //     icon: 'assets/ipf/icons/new/7.png',
    //     name: 'Attendees', 
    //     analytics: ''
    // },
    {
        path: '/login',
        icon: 'assets/ipf/icons/new/10.png',
        name: 'exit',
        analytics: 'click_logout'
    },
];
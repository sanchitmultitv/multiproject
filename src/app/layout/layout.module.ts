import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './header/header.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { HelpDeskComponent } from './components/help-desk/help-desk.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpeakersComponent } from './components/speakers/speakers.component';
import { splitAtColon } from '@angular/compiler/src/util';
import { DatepickerModule } from 'ng2-datepicker';
import { PollComponent } from './poll/poll.component';
import { KbcComponent } from './kbc/kbc.component';
 import{ScheduleCallComponent}  from './schedule-call/schedule-call.component';
import { AppoinmentsComponent } from './appoinments/appoinments.component';
import { AttendeesComponent } from './attendees/attendees.component'
//  import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


// import { from } from 'core-js/fn/array';

@NgModule({
  declarations: [LayoutComponent, HeaderComponent, AgendaComponent, FeedbackComponent, HelpDeskComponent,ScheduleCallComponent, SpeakersComponent, PollComponent, KbcComponent, AppoinmentsComponent, AttendeesComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule,
     DatepickerModule ,
     NgbModule

  ],
    // exports:[DatepickerModule]
})
export class LayoutModule { }

import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SteupServiceService } from 'src/app/services/steup-service.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';

declare var introJs: any;
declare var $: any;

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, AfterViewInit, OnDestroy {
  intro: any;
  receptionEnd = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  networkingLounge = false;
  showVideo = false;
  relatedPdf;
  showQuiz = false;
  result: any;
  selectedArray: any = [];
  quizData: any = [];
  quizlist = [];
  summaryList = [];
  summaryBoolean = false;
  index = 0;
  sumIndex = 0;
  user_id: any;
  quizStatus = 'Quiz';
  correctAnsCount = 0;
  liveMsg= false;
  pages = [
    // { id: 1, path: '/gamezone', analytics: 'click_engangementzone', pulseId: 'gameZone'},
    // { id: 2, path: '/eventhall', analytics: 'auditorium_226', pulseId: 'eventPulse' },
    // { id: 3, path: '/eventhalltwo', analytics: 'auditorium_227', pulseId: 'eventtwoPulse' },
    // { id: 5, path: '/gratitudewall', analytics: 'click_gallery', pulseId: 'gratitudePulse' },
    // { id: 4, path: '/photobooth', analytics: 'click_photobooth', pulseId: 'photobooth' },
    // {id:8, path:'/gratitudewall', analytics: 'click_gratitudewall'}
  ];
  allPdfs = [
    // {id:1, pdf:'../../../assets/spmcil/pdf/Standee-03.pdf'},
    // {id:4, pdf:'../../../assets/spmcil/pdf/Standee-04.pdf'},
    // {id:7, pdf:'../../../assets/spmcil/pdf/Standee-02.pdf'},
    // {id:8, pdf:'assets/ipf/images/agenda/agenda.pdf'},
  ];
  popups = [
    // attendeesModal
    //  {id:5, datatarget:'agendaModal', analytics: 'click_agenda', pulseId: 'agendaPulse'},
    // { id: 6, datatarget: 'helpDeskModal', analytics: 'click_helpdesk', pulseId: 'helpdeskPulse' },
    //  {id:7, datatarget:'speakersModal', analytics: 'click_speakerprofile'},
   
    // { id: 8, datatarget: 'quizModel', analytics: '', pulseId: 'quizPules' },


  ];
  speaker;
  constructor(private router: Router, private _analytics: SteupServiceService, private sanitiser: DomSanitizer,private _fd: FetchDataService,) { }

  ngOnInit(): void {

    
    this.stepup('click_lobby');
    let videoplay: any = document.getElementById('lobbyBgVideo');
    videoplay.play();
    let centerPlay: any = document.getElementById('centerPlay');
    centerPlay.play();
    this.speaker = JSON.parse(localStorage.getItem('virtual')).category;
    
    this.getUserguide();

  }
 
  
  videoPlay(data) {
    // alert(data)
    //let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    // alert(timer)
    // if (timer >= '18:59:59') {
    if (data === 'click_engangementzone') {
      // alert(data)
      this.networkingLounge = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'auditorium_224') {
      // alert(data)

      this.receptionEnd = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_gallery') {
      // alert(data)

      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    if (data === 'click_photobooth') {
      // alert(data)

      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById(data);
      vid.play();
    }
    // this.receptionEnd = true;
    // this.auditoriumLeft = true;
    // this.auditoriumRight = true;
    // this.networkingLounge = true;
    // this.showVideo = true;
    // let vid: any = document.getElementById(data);
    // vid.play();
    // }else {
    //   $('.note3').modal('show');
    // }

    // let pauseVideo: any = document.getElementById("bhudha");
    //   pauseVideo.currentTime = 0;
    //   pauseVideo.pause();
  }
  selectedOption;
  optionIndex = -1;
  color;
  noColor = '#27272d';

  closePopup() {
    $('.quizModal').modal('hide');
  }
  openCallBox() {
    $('.scheduleCallmodal').modal('show');
  }

  receptionEndVideo() {
    this.router.navigate(['/eventhall']);

  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/photobooth']);
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/gratitudewall']);
  }
  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/gamezone']);
  }

  stepup(action) {
    this._analytics.stepUpAnalytics(action);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.getUserguide();
    }, 1000);
  }
  getUserguide() {

    this.intro = introJs().setOptions({
      hidePrev: true,
      hideNext: true,
      exitOnOverlayClick: false,
      exitOnEsc: false,
      steps: [
        {
          element: document.querySelectorAll("#helpdeskPulse")[0],
          intro: "<div style='text-align:center'>For any queries Relating to the event please visit the Tech Support</div>"
        },
        {
          element: document.querySelectorAll("#eventPulse")[0],
          intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule</div>"
        },
        {
          element: document.querySelectorAll("#gratitudePulse")[0],
          intro: "<div style='text-align:center'>Click here to see the gallery</div>"
        },
        {
          element: document.querySelectorAll("#gameZone")[0],
          intro: "<div style='text-align:center'>Click here to play the games</div>"
        },
        {
          element: document.querySelectorAll("#scheduleCallPules")[0],
          intro: "<div style='text-align:center'>Click here to capture pictures</div>"
        },
        {
          element: document.querySelectorAll("#photobooth")[0],
          intro: "<div style='text-align:center'>Click here to capture pictures</div>"
        },
      ]
    }).oncomplete(() => document.cookie = "intro-complete=true");


    let start = () => this.intro.start();
    start();
    // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
    //   window.setTimeout(start, 1000);
  }
  openPdf(pdf) {
    this.relatedPdf = this.sanitiser.bypassSecurityTrustResourceUrl(pdf);
    $('#lobbyModal').modal('show');

  }
  
  ngOnDestroy() {
    if (localStorage.getItem('user_guide') === 'start') {
      let stop = () => this.intro.exit();
      stop();
    }
    localStorage.removeItem('user_guide');
  }
}

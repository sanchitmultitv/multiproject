import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GratitudeWallRoutingModule } from './gratitude-wall-routing.module';
import { GratitudeWallComponent } from './gratitude-wall.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgStreamingModule } from 'videogular2/compiled/streaming';

@NgModule({
  declarations: [GratitudeWallComponent],
  imports: [
    CommonModule,
    GratitudeWallRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    VgStreamingModule
  ]
})
export class GratitudeWallModule { }

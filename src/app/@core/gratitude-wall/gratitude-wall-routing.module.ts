import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GratitudeWallComponent } from './gratitude-wall.component';


const routes: Routes = [
  {
    path: '', component: GratitudeWallComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GratitudeWallRoutingModule { }

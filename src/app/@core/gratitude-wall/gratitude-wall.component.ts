import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as Clappr from 'clappr';
declare var $:any;

@Component({
  selector: 'app-gratitude-wall',
  templateUrl: './gratitude-wall.component.html',
  styleUrls: ['./gratitude-wall.component.scss']
})
export class GratitudeWallComponent implements OnInit {
  document:any;
  constructor(private sanitiser: DomSanitizer) { }
  videoPlayer = 'https://d3ep09c8x21fmh.cloudfront.net/fasenra/gallery.mp4';
  ngOnInit(): void {
  }
  showpdf(id){
    this.document = this.sanitiser.bypassSecurityTrustResourceUrl(`assets/spmcil/galleryPdf/standee-0${id}.pdf`)
    $('#galleryModal').modal('show');
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpeakerProfileComponent } from './speaker-profile.component';


const routes: Routes = [
  {path:'', component: SpeakerProfileComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpeakerProfileRoutingModule { }

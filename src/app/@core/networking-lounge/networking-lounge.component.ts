import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ToastrService } from 'ngx-toastr';

declare var $: any;

@Component({
  selector: 'app-networking-lounge',
  templateUrl: './networking-lounge.component.html',
  styleUrls: ['./networking-lounge.component.scss']
})
export class NetworkingLoungeComponent implements OnInit {
  relatedPdf;
  videoEnd = false;
  liveMsg = false;
  ChatMsg = false;
  senderName;
  resetLINKS: any;
  sender_id: any;
  sender_name;
  searchChatList = [];
  receiver_id: any;
  chatMessage = [];
  allChatList = [];
  timer;
  commentsList = [];
  commentsListing = [];
  textMessage = new FormControl('');
  type = new FormControl('');
  interval;
  typ = 'normal';
  oneToOneChatList = [];
  allChatIndex = 0;
  chatUser: any;
  receiver_name;
  name;
  email;
  data: any;
   actives: any = [];
  
  pages = [
   
    { id: 1, path: '/photobooth', analytics: 'click_photobooth', pulseId: 'photobooth' },
  ];
  allPdfs = [
  
    {id:2, pdf:'assets/ipf/images/agenda/agenda.pdf'},
  ];
  popups = [
    // { id: 3, datatarget: 'chatsModalModal', analytics: 'click_helpdesk', pulseId: 'helpdeskPulse' },
   


  ];
  constructor(private sanitiser: DomSanitizer,private _fd: FetchDataService,private router: Router, private chat: ChatService,private toastr: ToastrService) { }

  ngOnInit(): void {
    let datas = JSON.parse(localStorage.getItem('virtual'));
    this.name = datas.name;
    this.email = datas.email;
    this.networksend();
    this.getAllAttendees();

    // this.getAllAttendees()
    this.chat.getconnect('toujeo-164');
    this.chat.getMessages().subscribe((data => {
      //  console.log('data',data);
      let check = data.split('_');
      console.log(data, 'okkk');
      let datas = JSON.parse(localStorage.getItem('virtual'));
      this.chatUser = check[2]; 
      if (check[0] == 'one2one' && check[1] == datas.id) {
        
        this.toastr.success(this.chatUser + ' sent you a message');
          $('.chatsModal').modal('show');
        
        // this.toastr.onHidden = function() { console.log("onHide"); };

        this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;
        });
      }
      if (data == 'start_live') {
        this.liveMsg = true;
      }
      if (data == 'stop_live') {
        this.liveMsg = false;
      }
      else {
        let getMsg = data.split('_');
        console.log('chats', getMsg);
        // if (getMsg[0] == "one" && getMsg[1] == "to" && getMsg[2] == "one") {
        //   let data = JSON.parse(localStorage.getItem('virtual'));
        //   if (getMsg[3] == data.id) {
        //     this.senderName = getMsg[4];
        //     this.ChatMsg = true;
        //     setTimeout(() => {
        //       this.ChatMsg = false;
        //     }, 5000);
        //   }
        // }
       
        let datas = JSON.parse(localStorage.getItem('virtual'));
      this.chatUser = getMsg[2];
        if(getMsg[0] == "one2one" && getMsg[1] == datas.id){
        // this.toastr.success(this.chatUser + ' sent you a message');
          // this.senderName = getMsg[2];
          // this.ChatMsg = true;
          //    setTimeout(() => {
          //      this.ChatMsg = false;
          //   }, 5000);
          // if(getMsg[3]== data.id){
            
           
         
          // }
        }
      }
    }));
    
  
    
  }
  networksend() {
    let timer: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    this.resetLINKS = this.router.url;
     let data = JSON.parse(localStorage.getItem('virtual'));

    const formData = new FormData();
     formData.append('event_id', '164');
     formData.append('user_id', data.id );
     formData.append('name', data.name );
     formData.append('email', data.email );
     formData.append('page', this.resetLINKS );
     formData.append('created', timer);

    // let data = JSON.parse(localStorage.getItem('virtual'));
    // this.resetLINKS = this.router.url;

    // console.log("707007"+this.router.url);
    this._fd.postNetwork(formData).subscribe((res => {
      if (res.code === 1) {
        // alert('Submitted Succesfully');
      }
    }))

  }
  getAllAttendees() {

    let event_id = 164;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this._fd.GetNetworkpage(event_id).subscribe((res: any) => {
      this.allChatList = res.result;
      this.searchChatList = res.result;
      this.receiver_id = res.result[0].user_id;
      this.receiver_name = res.result[0].name;
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;
      });

      // this.timer = setInterval(() => {
      //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //     this.chatMessage = res.result;
      //   });

      // }, 150000);
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;
      });

    });
   //$('.chatsModal').modal('show');

  }
  openChat(){
    $('.chatsModal').modal('show');
  }
  // closeAgenda(){
  //   $('.agendaModal').modal('hide');
  // }

  audiActive() {
    this._fd.activeAudi().subscribe(res => {
      this.actives = res.result;
    })
  }
  searchElement(query) {
    let event_id = 164;
    this._fd.getOne2oneChatList(event_id, query).subscribe(res => {
      this.allChatList = res.result;
    });
  }

  selectedChat(chat, ind) {
    // alert(chat)
    // alert(ind)
    this.allChatIndex = ind;
    let event_id = 164;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this.receiver_id = chat.user_id;
    this.receiver_name = chat.name;
    console.log(chat)
    this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      this.chatMessage = res.result;
    });
  }
  postOneToOneChat(event) {
    let msg = event.value;
    const formData = new FormData();
    formData.append('sender_id', this.sender_id);
    formData.append('sender_name', this.sender_name);
    formData.append('receiver_id', this.receiver_id);
    formData.append('receiver_name', this.receiver_name);
    formData.append('msg', msg);
    if (event.value !== null) {
      this._fd.postOne2oneChat(formData).subscribe(data => {
        this.textMessage.reset();
        this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;
          // alert("hy")
 console.log(this.chatMessage)
        });
      });
    }
    setTimeout(() => {
      $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight - 100;
    }, 1500);
    // this.timer = setInterval(() => {
    //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //     this.chatMessage = res.result;
    //   });
    // }, 1000);
  }
 
  // openPdf(pdf) {
  //   this.relatedPdf = this.sanitiser.bypassSecurityTrustResourceUrl(pdf);
  //   $('#lobbyModal').modal('show');

  // }
  getComments() {
    let event_id = 164;
    let user_id = 1;
    let type = 'normal';
    this._fd.getComments(event_id, user_id, type).subscribe(res => {
      this.commentsList = res.result;
      this.commentsListing = res.result;
    });
  }
  getType(value) {
    console.log(value);
    let event_id = 164;
    let user_id = 1;
    this.typ = value;
    this._fd.getComments(event_id, user_id, value).subscribe(res => {
      this.commentsList = res.result;
    })
  }
  closePopup() {
    $('.chatsModal').modal('hide');
  }

}

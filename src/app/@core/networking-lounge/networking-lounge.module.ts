import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NetworkingLoungeRoutingModule } from './networking-lounge-routing.module';
import { NetworkingLoungeComponent } from './networking-lounge.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NetworkingLoungeRoutingModule,
    NetworkingLoungeComponent,
    FormsModule,
    ReactiveFormsModule, 

  ]
})
export class NetworkingLoungeModule { }

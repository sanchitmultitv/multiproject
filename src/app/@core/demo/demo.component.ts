import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  constructor(private router: Router, private chat: ChatService, private _fd: FetchDataService){ }
  img = [];
   
   isImageLoading 
  ngOnInit(): void {
    this.getImage();
  }

  getImage(){
    this._fd.getCaptureImage().subscribe(res => {
      this.img = res.result;
     
  
  });
   
}
  

  }

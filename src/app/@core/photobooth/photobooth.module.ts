import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotoboothRoutingModule } from './photobooth-routing.module';
import { PhotoboothComponent } from './photobooth.component';


@NgModule({
  declarations: [PhotoboothComponent],
  imports: [
    CommonModule,
    PhotoboothRoutingModule
  ]
})
export class PhotoboothModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotoboothComponent } from './photobooth.component';


const routes: Routes = [
  {
    path:'', component: PhotoboothComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoboothRoutingModule { }

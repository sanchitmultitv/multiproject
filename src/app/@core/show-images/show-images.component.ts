import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-show-images',
  templateUrl: './show-images.component.html',
  styleUrls: ['./show-images.component.scss']
})
export class ShowImagesComponent implements OnInit {

  constructor( private _fd:FetchDataService) { }
  images:any;
  ngOnInit(): void {
    this.getImage();
  }

  getImage(){
    this._fd.getCaptureImage().subscribe(res => {
      this.images = res.result;
      console.log(this.images);
    });
  }
}

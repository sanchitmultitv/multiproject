import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service'
 import { fadeAnimation } from 'src/app/shared/animation/fade.animation';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms'
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-event-hall-two',
  templateUrl: './event-hall-two.component.html',
  styleUrls: ['./event-hall-two.component.scss'],
  // animations: [fadeAnimation],
})
export class EventHallTwoComponent implements OnInit {
  player:any;
  interval;
  // player:any
  user_name;
  newWidth;
  newHeight;
   videoPlayer;
   quizData: any = [];
  quizlist = [];
  summaryList = [];
  summaryBoolean = false;
  index = 0;
  sumIndex = 0;
  user_id: any;
  quizStatus = 'Quiz';
  correctAnsCount = 0;
  liveMsg= false;
  constructor(private router: Router, private _fd:FetchDataService,private chatService: ChatService) { }

  ngOnInit(): void {
    let event_id = 164;
    this._fd.getPlayerUrl(event_id).subscribe((res:any)=>{
      let stream = res.result[0]['stream'];
      this.playVideo(stream);
    });
      let data = JSON.parse(localStorage.getItem('virtual'));
    this.user_name = data.name;
    this.getAudi();
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
         this.loadData();
    this.chatService.getconnect('toujeo-164');
    this.chatService.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'groupchat') {
        this.gotoChat();
      }
    }));

      }
      playVideo(stream) {
        var playerElement = document.getElementById("player-wrapper");
        this.player = new Clappr.Player({
          parentId: 'player-wrapper',
          source: stream,
          poster: 'assets/ipf/images/login-bg.jpg',
          maxBufferLength: 30,
          width: '100%',
          // height: '500',
          // width: window.innerWidth*.5781,
          height: window.innerWidth*.5707/16*9,
          autoPlay: true,
          hideMediaControl: true,
          hideVolumeBar: true,
          hideSeekBar: true,
          persistConfig: false,
          // chromeless: true,
          // mute: true,
          visibilityEnableIcon: false,
          disableErrorScreen: true,
          playback: {
            playInline: true,
            // recycleVideo: Clappr.Browser.isMobile,
            recycleVideo: true
          },
        });
        this.player.attachTo(playerElement);    
      }
      getHeartbeat(){
        // alert("ss")
        let data = JSON.parse(localStorage.getItem('virtual'));
        const formData = new FormData();
        formData.append('user_id', data.id );
        formData.append('event_id', '164');
        formData.append('audi', '227');
        this._fd.heartbeat(formData).subscribe(res=>{
          console.log(res);
        })
      }
      getAudi(){
        this._fd.getMyAudi('227').subscribe(res=>{
          console.log('');
          this.videoPlayer = res.result[0].stream;
        })
        }
        textMessage = new FormControl('',[Validators.required]);
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'multiproject';
  serdia_room = localStorage.getItem('multiproject');
  loadData() {
    this. gotoChat();
    this.chatService.getconnect('toujeo-164');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.roomName);
    localStorage.setItem('username', data.name);
    this.chatService.receiveMessages(this.roomName).subscribe((msgs: any) => {
      if (msgs.roomId === 1) {
        this.messageList.push(msgs);
      }
      console.log('demo', this.messageList);
    });
  }
      gotoChat(){
      
        this.router.navigate(['/eventhalltwo']);
      }
      playAudioClap() {
        let playaudio: any = document.getElementById('myAudioClap');
        playaudio.play();
      }
      clapping(){
        let clap: any = document.getElementById('myAudioClap');
        clap.play();
      }
      whistling(){
        let whistle: any = document.getElementById('myAudioWhistle');
        whistle.play();
      }
      // gotoChat(){
      //   this.router.navigate(['/eventhalltwo/chat']);
      // }

      
      @HostListener('window:resize', ['$event']) onResize(event) {
        this.player.resize({ width: window.innerWidth*.5781, height: window.innerWidth*.5707/16*9 });
      }
}

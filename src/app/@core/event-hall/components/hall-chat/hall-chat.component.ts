import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
@Component({
  selector: 'app-hall-chat',
  templateUrl: './hall-chat.component.html',
  styleUrls: ['./hall-chat.component.scss']
})
export class HallChatComponent implements OnInit, AfterViewInit {
  
  constructor(private router: Router, private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
     
  }
 
  ngAfterViewInit() {
    
    
  }
 
 
  
}

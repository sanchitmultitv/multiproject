import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventHallChatComponent } from './components/event-hall-chat/event-hall-chat.component';
import { EventHallTwoComponent } from './components/event-hall-two/event-hall-two.component';
import { HallChatComponent } from './components/hall-chat/hall-chat.component';
import { QnaComponent } from './components/qna/qna.component';
import { EventHallComponent } from './event-hall.component';



const routes: Routes = [

  {
    path: '',
    component: EventHallComponent,


    children: [

      {
        path: 'eventHallChat', component: EventHallChatComponent
      },
      {
        path: 'chat', component: HallChatComponent
      },
      {
        path: 'qna', component: QnaComponent
      }
    ]
  },
  {
    path: 'eventhalltwo', component: EventHallTwoComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventHallRoutingModule { }

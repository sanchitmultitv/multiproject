import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeboyComponent } from './lifeboy.component';

describe('LifeboyComponent', () => {
  let component: LifeboyComponent;
  let fixture: ComponentFixture<LifeboyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeboyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeboyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

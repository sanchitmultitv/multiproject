import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExhibitionHallComponent } from './exhibition-hall.component';

describe('ExhibitionHallComponent', () => {
  let component: ExhibitionHallComponent;
  let fixture: ComponentFixture<ExhibitionHallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExhibitionHallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExhibitionHallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
@Component({
  selector: 'app-exhibit-life',
  templateUrl: './exhibit-life.component.html',
  styleUrls: ['./exhibit-life.component.scss']
})
export class ExhibitLifeComponent implements OnInit {
  
  constructor(private router: Router, private chat: ChatService) { }

  ngOnInit(): void {
  }
  gotoCare(){
    this.router.navigate(['/exhibitionHall/care']);
     }
  gotoCommitToSucceed(id){
    this.router.navigate(['/exhibition-hall/booths', id]);
  }
  gotoDareToInnovative(id){
    this.router.navigate(['/exhibition-hall/dareToInnovative', id]);
  }
  gotoGrowBySharing(){
    this.router.navigate(['/exhibitionHall/growBySharing']);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AriseComponent } from './arise.component';

describe('AriseComponent', () => {
  let component: AriseComponent;
  let fixture: ComponentFixture<AriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

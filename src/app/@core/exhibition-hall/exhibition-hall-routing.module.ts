import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExhibitionHallComponent } from './exhibition-hall.component';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { AdaptComponent } from './adapt/adapt.component';
import { AriseComponent } from './arise/arise.component';
import { AchiveComponent } from './achive/achive.component';
import { GrowBySharingComponent } from './grow-by-sharing/grow-by-sharing.component';

const routes: Routes = [
  {path:'', component:ExhibitionHallComponent,
  children:[
    {path:'', redirectTo:'life' },
    {path:'life', component:ExhibitLifeComponent},
    {path:'lifeboy', component:LifeboyComponent},
    {path:'care/:id', component:AdaptComponent},
    {path:'booths/:id', component:AriseComponent},
    {path:'dareToInnovative/:id', component:AchiveComponent},
    {path:'growBySharing', component:GrowBySharingComponent},
]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionHallRoutingModule { }

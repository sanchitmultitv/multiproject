import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExhibitionHallRoutingModule } from './exhibition-hall-routing.module';
import { AchiveComponent } from './achive/achive.component';
import { AdaptComponent } from './adapt/adapt.component';
import { AriseComponent } from './arise/arise.component';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { GrowBySharingComponent } from './grow-by-sharing/grow-by-sharing.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
// import { LayoutModule } from 'src/app/layout/layout.module';
// import { from } from 'core-js/fn/array';
 import {DatepickerModule }from 'ng2-datepicker'
//  import { ScheduleCallComponent } from 'src/app/layout/components/schedule-call/schedule-call.component';
@NgModule({
  declarations: [AchiveComponent, AdaptComponent, AriseComponent, ExhibitLifeComponent, GrowBySharingComponent, LifeboyComponent,],
  imports: [
    CommonModule,
    ExhibitionHallRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    FormsModule,
    ReactiveFormsModule,
    DatepickerModule,
    // LayoutModule,
  ]
})
export class ExhibitionHallModule { }

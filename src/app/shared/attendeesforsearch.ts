export const coreSelectorArr=[
  {'name': 'Select Sector', 'value': 'Select Sector'},
  {'name': 'Solar', 'value': 'Solar'},
  {'name': 'Wind / Offshore Wind', 'value': 'Wind'},
  {'name': 'Bio-Energy', 'value': 'Bio-Energy'},
  {'name': 'Hydro / SHP, Hybrid RE', 'value': 'Hydro'},
  {'name': 'Power', 'value': 'Power'},
  {'name': 'Future Mobility and Charging Infra', 'value': 'Future Mobility and Charging Infra'},
  {'name': 'Energy Storage', 'value': 'Energy Storage'},
  {'name': 'Energy Efficiency', 'value': 'Energy Efficiency'},
  {'name': 'New / Alternative Energy(Hydrogen, / GeoThermal, Tidal, etc,)', 'value': 'Alternative Energy'},
  {'name': 'Others', 'value': 'Others'},
  ];
  export const coreBusinessArr=[
    {'name': 'Select Business', 'value': 'Select Business'},
    {'name': 'Governance', 'value': 'Governance'},
    {'name': 'Project Development', 'value': 'Project Development'},
    {'name': 'Manufacturing', 'value': 'Manufacturing'},
    {'name': 'Financing', 'value': 'Financing'},
    {'name': 'Consulting', 'value': 'Consulting'},
    {'name': 'Academica', 'value': 'Academica'},
    {'name': 'R&D', 'value': 'Research'},
    {'name': 'Other', 'value': 'Other'},
  ];
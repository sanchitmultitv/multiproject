import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-animate',
  templateUrl: './animate.component.html',
  styleUrls: ['./animate.component.scss']
})
export class AnimateComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  endVideo(){
    this.router.navigate(['/lobby']);
  }
  skip(){
    let vid: any = document.getElementById('loginVideo');
    vid.pause();
    vid.currentActiveTime = 0;
    this.router.navigate(['/lobby']);
  }
}
